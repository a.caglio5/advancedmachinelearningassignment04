import os
from tensorflow.keras.models import Model
from tensorflow.keras.applications.vgg16 import VGG16
from assignment04.functions import load_data, model_definition, plot_accuracy_over_cuts, plot_history
from sklearn.model_selection import train_test_split

"""Data directory"""

current_directory = os.getcwd()
data_directory = current_directory + "/simpsons_dataset"
train_directory = data_directory + "/train"
test_directory = data_directory + "/test"

"""Classes definition"""

class_names = ['apu', 'bart', 'krusty', 'marge', 'winchester']
class_names_label = {class_name: i for i, class_name in enumerate(class_names)}

number_of_classes = len(class_names)

"""Load of the input images"""

(train_images, train_labels), (test_images, test_labels) = load_data(class_names_label, train_directory, test_directory)

"""Data initialization"""

score_train = {}
score_validation = {}
score_test = {}

cuts = ['block1_pool', 'block2_pool', 'block3_conv3', 'block4_pool', 'block5_conv2', 'last_layer_available']

"""Features extraction, classifier definition, classifier training, classifier evaluation over cuts"""

for cut in cuts:

    """Cut at specific layer of VGG16"""

    if cut == 'last_layer_available':
        model = VGG16(weights='imagenet', include_top=False)
        model.summary()
    else:
        base_model = VGG16(weights='imagenet')
        model = Model(inputs=base_model.input, outputs=base_model.get_layer(cut).output)
        model.summary()

    """Features extraction"""

    train_features = model.predict(train_images)
    print("\n\nShape of features of train")
    print(train_features.shape)

    test_features = model.predict(test_images)
    print("\n\nShape of features of test")
    print(test_features.shape)

    # reshape delle features
    train_features = \
        train_features.reshape(
            (train_features.shape[0], train_features.shape[1] * train_features.shape[2] * train_features.shape[3]))
    print("\n\nShape of features of train after reshape")
    print(train_features.shape)

    test_features = \
        test_features.reshape(
            (test_features.shape[0], test_features.shape[1] * test_features.shape[2] * test_features.shape[3]))
    print("\n\nShape of features of test after reshape")
    print(test_features.shape)

    """Split data into train/validation set"""

    train_features, validation_features, train_labels_after_split, validation_labels = \
        train_test_split(train_features, train_labels, test_size=0.1, random_state=0)

    """Classic classifier definition"""

    classic_classifier = model_definition(train_features.shape[1], number_of_classes)

    classic_classifier.summary()

    """Classic classifier training"""

    batch_size = 16
    epochs = 20

    classic_classifier.compile(loss="sparse_categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
    classic_classifier_history = classic_classifier.fit(train_features, train_labels_after_split,
                                                        batch_size=batch_size,
                                                        epochs=epochs,
                                                        verbose=1,
                                                        validation_data=(validation_features, validation_labels))

    # Plot loss and accuracy on training and validation set
    x_plot = list(range(1, epochs + 1))
    plot_history(x_plot, classic_classifier_history, cut)

    """Classic classifier evaluation"""

    score_train[cut] = classic_classifier.evaluate(train_features, train_labels_after_split, verbose=0)
    print("Train loss of cut at", cut, ":", score_train[cut][0])
    print("Train accuracy of cut at", cut, ":", score_train[cut][1])

    score_validation[cut] = classic_classifier.evaluate(validation_features, validation_labels, verbose=0)
    print("Validation loss of cut at", cut, ":", score_validation[cut][0])
    print("Validation accuracy of cut at", cut, ":", score_validation[cut][1])

    score_test[cut] = classic_classifier.evaluate(test_features, test_labels, verbose=0)
    print("Test loss of cut at", cut, ":", score_test[cut][0])
    print("Test accuracy of cut at", cut, ":", score_test[cut][1])

print("\n\nSCORE TRAIN OVER CUTS", score_train)
print("\n\nSCORE VALIDATION OVER CUTS", score_validation)
print("\n\nSCORE TEST OVER CUTS", score_test)

"""Plot accuracy over cut"""

# train accuracy
train_accuracies = [score_train['block1_pool'][1], score_train['block2_pool'][1],
                    score_train['block3_conv3'][1], score_train['block4_pool'][1],
                    score_train['block5_conv2'][1], score_train['last_layer_available'][1]]
plot_accuracy_over_cuts(x_plot_labels=cuts, y_plot=train_accuracies, title='Train')

# validation accuracy
validation_accuracies = [score_validation['block1_pool'][1], score_validation['block2_pool'][1],
                         score_validation['block3_conv3'][1], score_validation['block4_pool'][1],
                         score_validation['block5_conv2'][1], score_validation['last_layer_available'][1]]
plot_accuracy_over_cuts(x_plot_labels=cuts, y_plot=validation_accuracies, title='Validation')

# test accuracy
test_accuracies = [score_test['block1_pool'][1], score_test['block2_pool'][1],
                   score_test['block3_conv3'][1], score_test['block4_pool'][1],
                   score_test['block5_conv2'][1], score_test['last_layer_available'][1]]
plot_accuracy_over_cuts(x_plot_labels=cuts, y_plot=test_accuracies, title='Test')
