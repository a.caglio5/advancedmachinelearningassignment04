import os
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.vgg16 import preprocess_input
from tensorflow import keras
from tensorflow.keras import layers


def load_data(class_names_label, train_directory, test_directory):

    datasets = [train_directory, test_directory]
    output = []
    counter = 0
    # Iterate through training and test sets
    for dataset in datasets:

        images = []
        labels = []

        print("Loading {}".format(dataset))

        # Iterate through each folder corresponding to a category
        for folder in os.listdir(dataset):
            label = class_names_label[folder]
            # Iterate through each image in our folder
            for file in tqdm(os.listdir(os.path.join(dataset, folder))):
                # Get the path name of the image
                img_path = os.path.join(os.path.join(dataset, folder), file)

                # Open and resize the img
                img = image.load_img(img_path, target_size=(224, 224))
                img = image.img_to_array(img)
                img = preprocess_input(img)

                # Append the image and its corresponding label to the output
                images.append(img)
                labels.append(label)

                counter = counter + 1

        images = np.array(images, dtype='float32')
        labels = np.array(labels, dtype='int32')

        output.append((images, labels))

    return output


def model_definition(input_shape, num_classes):
    model = keras.Sequential(
        [
            keras.Input(shape=input_shape),
            layers.Dense(64, activation="relu"),
            layers.Dense(64, activation="relu"),
            layers.Dense(num_classes, activation="softmax"),
        ]
    )

    return model


def plot_history(x_plot, model_history, title):
    plt.figure()
    plt.title(title + ' Model Loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')

    plt.plot(x_plot, model_history.history['loss'])
    plt.plot(x_plot, model_history.history['val_loss'])
    plt.legend(['Training', 'Validation'])

    plt.figure()
    plt.title(title + ' Model Accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')

    plt.plot(x_plot, model_history.history['accuracy'])
    plt.plot(x_plot, model_history.history['val_accuracy'])
    plt.legend(['Training', 'Validation'], loc='lower right')

    plt.show()


def plot_accuracy_over_cuts(x_plot_labels, y_plot, title):
    plt.figure()
    plt.title(title + ' accuracy over cuts')
    plt.xlabel('Cuts')
    plt.ylabel('Accuracy')

    plt.plot(range(1, len(x_plot_labels) + 1), y_plot)
    plt.legend(['Accuracy over cuts'])

    plt.xticks(range(1, len(x_plot_labels) + 1), x_plot_labels)

    plt.show()
